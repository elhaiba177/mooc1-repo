>**Quiz** :pencil: : introduction à la programmation avec python turtle

**Q1** : Un programme est un :

- [ ] Ensemble d'instructions
- [ ] Ensemble d'ordres
- [ ] Un fichier

**Q2** : LOGO est un :
- [ ] Programme de dessin
- [ ] Langage de programmation
- [ ] jeu


**Q3** : La primitive BC permet de :
- [ ] Vider l'ecran
- [ ] Baisser le crayon
- [ ] Cacher la tortue 


**Q4** : La primitive carre permet de :
- [ ] Dessiner un carré
- [ ] Dessiner un cercle
- [ ] aucune des deux

**Q5** : Les instructions Xlogo s’exécutent dans :

- [ ] La ligne de commande 
- [ ] l’éditeur
- [ ] la fenêtre graphique


**Q6** : Une procédure se délimite par :

- [ ] Début …………..Fin
- [ ] Pour…….Fin
- [ ] To………End


**Q7** : Quelle est l’écriture correcte :
```java
ftc 5 fcc rouge Lc fpos [60 30] bc fpos [60 186] fpos [280 186] fpos [280 30] fcc bleu fpos [280 -180]fpos [280 30] fcc rouge fpos[60
30] lc fpos [120 120] bc remplis fcc vertfonce fpos [ 220 120 ] fpos [ 139 61 ] fpos [ 170 156] fpos [ 200 61] fpos [120 120] ct

```
- [ ] drapeau marocain 
- [ ] carré
- [ ] drapeau français 

