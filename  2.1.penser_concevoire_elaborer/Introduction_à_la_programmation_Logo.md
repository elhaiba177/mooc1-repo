**Thématique** :Introduction à la programmation Logo

**Notions liées** :Tortue et graphisme,Application numérique,Fonctionnement de l'évaluateur

**Résumé de l’activité** :les élèves sont invités à donner des instructions.

**Objectifs** :Maîtriser le concept des instructions & Produire un programme informatique.

**Auteur** :RAIGAT ElHAIBA

**Durée de l’activité** :une heure .

**Forme de participation** :binôme, sur machine.

**Matériel nécessaire** :ordinateur

**Préparation** :préparation du scénario pédagogique

**Fiche élève cours** :Disponible [ici](https://drive.google.com/file/d/1yCGu453ATDfYtxNEEdFXXmlDwjr9j8NG/view?usp=sharing)

**Fiche élève activité** :Disponible [ici](https://drive.google.com/file/d/1boVKK1TQlw5wG0abeFL3YaxrESrhYIJr/view?usp=sharing)
