- **TP1**:  Instructions programmation LOGO

    - Matériel didactique :computer: : ordinateur, vidéo projecteur, tableau

    - Ressources didactique :orange_book: : Programming in LOGO
    
    - Activités enseignant: Présenter le travail à faire, intervenir pour aider les apprenants (orienter, guider, faciliter), distribuer les apprenants en binôme
    
    - Activités apprenants: Réaliser les activités, discuter les solutions avec les autres binômes, créer une carte mentale qui résume le concept de chaque activité ( *quoi ?*, *qui ?*, *quand ?*, *pourquoi ?*, *Comment ?* )


- **TP2**:  introduction à la programmation LOGO avec XLOGO

    - Matériel didactique :computer: : ordinateur, vidéo projecteur, tableau
    
    - Ressources didactique :blue_book: : XLogo: Manuel de référence / FICHES D’EXERCICES POUR X LOGO

    - Activités enseignant: Présenter le travail à faire, intervenir pour aider les apprenants (orienter, guider, faciliter), distribuer les apprenants en binôme

    - Activités apprenants: Réaliser les activités, discuter les solutions avec les autres binômes, créer une carte mentale qui résume le concept de chaque activité ( *quoi ?*, *qui ?*, *quand ?*, *pourquoi ?*, *Comment ?* )
